	//============================================================================

// Name        : restaurapp.cpp
// Author      : EL DOG Y EL RESTO :V
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include <cstring>
#include <cstdio>
#include <iostream>
#include <string>
#include <cstdlib>
#include <conio.h>
#include <fstream>
#include <vector>
#include <queue>
#define MAXV 100
#define oo 0x3f3f3f3f
using namespace std;
int ub;
struct Edge
{
	int node;
	int cost;
	Edge(int _node, int _cost) : node(_node), cost(_cost) {}
	Edge() : node(-1), cost(-1) {}
};

struct Graph
{
	vector<Edge> G[MAXV];
	int V;
	int E;
};

struct State
{
	int node;
	int cost;
	State(int _node, int _cost) : node(_node), cost(_cost) {}
	bool operator <(const State &b) const
	{
		return cost > b.cost;
	}
};

int algoritmo(const int begin, const int end, const Graph graph)
{
	priority_queue<State> pq;
	vector<int> Dist(graph.V, oo);
	vector<bool> mark(graph.V, false);

	Dist[begin] = 0;
	pq.push(State(begin, 0));
	while(!pq.empty())
	{
		State st = pq.top(); pq.pop();
		mark[st.node] = true;
		if (st.node == end)
			return st.cost;

		int T = (int)graph.G[st.node].size();
		for(int i = 0; i < T; ++i)
		{

			if (!mark[graph.G[st.node][i].node] && ((Dist[st.node] + graph.G[st.node][i].cost) < Dist[graph.G[st.node][i].node]))
			{
				Dist[graph.G[st.node][i].node] = st.cost + graph.G[st.node][i].cost;
				pq.push(State(graph.G[st.node][i].node, st.cost + graph.G[st.node][i].cost));
			}
		}
	}
	return -1;
}

struct Programa
{
	int V, E;
	int comienzo, fin;
	void definirGrafo(Graph& graph)
	{
		graph.V = 46;
		graph.E = 65;
	}

	void cargarGrafo(Graph & graph)
	{
			graph.G[0].push_back(Edge(1, 63));
			graph.G[1].push_back(Edge(0, 63));
			graph.G[1].push_back(Edge(2, 120));
			graph.G[2].push_back(Edge(1, 120));
			graph.G[1].push_back(Edge(4, 80));
			graph.G[4].push_back(Edge(1, 80));
			graph.G[2].push_back(Edge(3, 56));
			graph.G[3].push_back(Edge(2, 56));
			graph.G[1].push_back(Edge(8, 90));
			graph.G[8].push_back(Edge(1, 90));
			graph.G[4].push_back(Edge(8, 65));
			graph.G[8].push_back(Edge(4, 65));
			graph.G[4].push_back(Edge(5, 116));
			graph.G[5].push_back(Edge(4, 116));
			graph.G[3].push_back(Edge(8, 87));
	        graph.G[8].push_back(Edge(3, 87));
			graph.G[3].push_back(Edge(7, 100));
			graph.G[7].push_back(Edge(3, 100));
			graph.G[7].push_back(Edge(6, 70));
		    graph.G[6].push_back(Edge(7, 70));
			graph.G[5].push_back(Edge(6, 140));
			graph.G[6].push_back(Edge(5, 140));
			graph.G[5].push_back(Edge(8, 112));
			graph.G[8].push_back(Edge(5, 112));
			graph.G[12].push_back(Edge(6, 135));
			graph.G[6].push_back(Edge(12, 135));
			graph.G[12].push_back(Edge(11, 69));
			graph.G[11].push_back(Edge(12, 69));
			graph.G[11].push_back(Edge(7, 95));
			graph.G[7].push_back(Edge(11, 95));
			graph.G[15].push_back(Edge(12, 115));
			graph.G[12].push_back(Edge(15, 115));
			graph.G[15].push_back(Edge(14, 160));
			graph.G[14].push_back(Edge(15, 160));
			graph.G[7].push_back(Edge(10, 115));
			graph.G[10].push_back(Edge(7, 115));
			graph.G[10].push_back(Edge(15, 55));
			graph.G[15].push_back(Edge(10, 55));
			graph.G[9].push_back(Edge(14, 61));
			graph.G[14].push_back(Edge(9, 61));
			graph.G[9].push_back(Edge(10, 65));
			graph.G[10].push_back(Edge(9, 65));
			graph.G[11].push_back(Edge(15, 40));
			graph.G[15].push_back(Edge(11, 40));
			graph.G[12].push_back(Edge(20, 188));
			graph.G[20].push_back(Edge(12, 188));
			graph.G[20].push_back(Edge(18, 84));
			graph.G[18].push_back(Edge(20, 84));
			graph.G[18].push_back(Edge(17, 90));
			graph.G[17].push_back(Edge(18, 90));
			graph.G[17].push_back(Edge(16, 82));
			graph.G[16].push_back(Edge(17, 82));
			graph.G[15].push_back(Edge(16, 117));
			graph.G[16].push_back(Edge(15, 117));
			graph.G[18].push_back(Edge(19, 67));
			graph.G[19].push_back(Edge(18, 67));
			graph.G[18].push_back(Edge(21, 115));
			graph.G[21].push_back(Edge(18, 115));
			graph.G[21].push_back(Edge(22, 50));
			graph.G[22].push_back(Edge(21, 50));
			graph.G[23].push_back(Edge(22, 80));
			graph.G[22].push_back(Edge(23, 80));
			graph.G[23].push_back(Edge(24, 115));
			graph.G[24].push_back(Edge(23, 115));
			graph.G[24].push_back(Edge(19, 93));
			graph.G[19].push_back(Edge(24, 93));
			graph.G[23].push_back(Edge(25, 80));
			graph.G[25].push_back(Edge(23, 80));
			graph.G[23].push_back(Edge(27, 143));
			graph.G[27].push_back(Edge(23, 143));
			graph.G[25].push_back(Edge(26, 115));
			graph.G[26].push_back(Edge(25, 115));
			graph.G[25].push_back(Edge(27, 91));
			graph.G[27].push_back(Edge(25, 91));
			graph.G[23].push_back(Edge(26, 78));
			graph.G[26].push_back(Edge(23, 78));
			graph.G[27].push_back(Edge(26, 93));
			graph.G[26].push_back(Edge(27, 93));
			graph.G[24].push_back(Edge(30, 60));
			graph.G[30].push_back(Edge(24, 60));
			graph.G[28].push_back(Edge(30, 42));
			graph.G[30].push_back(Edge(28, 42));
			graph.G[28].push_back(Edge(29, 50));
			graph.G[29].push_back(Edge(28, 50));
			graph.G[29].push_back(Edge(30, 68));
			graph.G[30].push_back(Edge(29, 68));
			graph.G[29].push_back(Edge(27, 143));
			graph.G[27].push_back(Edge(29, 143));
			graph.G[29].push_back(Edge(32, 65));
			graph.G[32].push_back(Edge(29, 65));
			graph.G[27].push_back(Edge(37, 70));
			graph.G[37].push_back(Edge(27, 70));
			graph.G[33].push_back(Edge(37, 88));
			graph.G[37].push_back(Edge(33, 88));
			graph.G[37].push_back(Edge(38, 96));
			graph.G[38].push_back(Edge(37, 96));
			graph.G[36].push_back(Edge(38, 85));
			graph.G[38].push_back(Edge(36, 85));
			graph.G[36].push_back(Edge(34, 70));
			graph.G[34].push_back(Edge(36, 70));
			graph.G[33].push_back(Edge(34, 60));
			graph.G[34].push_back(Edge(33, 60));
			graph.G[32].push_back(Edge(34, 107));
			graph.G[34].push_back(Edge(32, 107));
			graph.G[32].push_back(Edge(31, 50));
			graph.G[31].push_back(Edge(32, 50));
			graph.G[32].push_back(Edge(40, 62));
			graph.G[40].push_back(Edge(32, 62));
			graph.G[40].push_back(Edge(39, 60));
			graph.G[39].push_back(Edge(40, 60));
			graph.G[40].push_back(Edge(41, 80));
			graph.G[41].push_back(Edge(40, 80));
			graph.G[35].push_back(Edge(41, 48));
			graph.G[41].push_back(Edge(35, 48));
			graph.G[34].push_back(Edge(35, 47));
			graph.G[35].push_back(Edge(34, 47));
			graph.G[46].push_back(Edge(35, 22));
			graph.G[35].push_back(Edge(46, 22));
			graph.G[41].push_back(Edge(45, 77));
			graph.G[45].push_back(Edge(41, 77));
			graph.G[45].push_back(Edge(44, 129));
			graph.G[44].push_back(Edge(45, 129));
			graph.G[44].push_back(Edge(43, 83));
			graph.G[43].push_back(Edge(44, 83));
			graph.G[43].push_back(Edge(42, 80));
			graph.G[42].push_back(Edge(43, 80));
			graph.G[41].push_back(Edge(42, 77));
			graph.G[42].push_back(Edge(41, 77));

	}
	int Dijkstra(Graph graph,int comienzo,int fin)
		{
			int n = algoritmo(comienzo, fin, graph);
			return n;
		}
	};
int calculoderutaminima(int inicio,int fin)
{
		Programa programa;
		Graph graph;


		programa.definirGrafo(graph);
		programa.cargarGrafo(graph);
		int distancia=programa.Dijkstra(graph,43,8);

 return distancia;
}
struct usuario{
	string nombre;
	string email;
	string nickname;
	string password;
};
struct comentarios{
	string usuario;
	string comentario;
};
struct calificacion{
	string usuario;
	string calificacion;
};
struct menu{
	string plato;
	string descripcion;
	string precio;
	string stock;
	int confirmar;

	vector<struct comentarios> comentarios;
};
struct restaurant{
	string nombre;
	string ubicacion;
	string telefono;
	string tipodecomida;
	vector<struct menu> menu;
	vector<struct calificacion> calificacion;
	float calificacionpromedio;
	int confirmar;
	int pos;
	float preciopromedio;
};
struct datosasociado{
	string nombre;
    string correo;
	string username;
	string password;
	vector<struct restaurant> restaurantes;
	int confirmar;
};
string userglb;
void sort1(struct restaurant* arr, int izq, int der){
	int i = izq, j = der;
	struct restaurant tmp;
	int p = arr [(izq + der)/2].calificacionpromedio;
	while (i <= j){
		while (arr[i].calificacionpromedio < p) i++;
		while (arr[j].calificacionpromedio > p) j--;
		if (i <= j){
			tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;
			i++; j--;
		}
	}
	if (izq < j)
		sort1(arr, izq ,j);
	if (i < der)
		sort1(arr, i ,der);
}
void sort2(struct restaurant* arr, int izq, int der){
	int i = izq, j = der;
	struct restaurant tmp;
	int p = arr [(izq + der)/2].preciopromedio;
	while (i <= j){
		while (arr[i].preciopromedio < p) i++;
		while (arr[j].preciopromedio > p) j--;
		if (i <= j){
			tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;
			i++; j--;
		}
	}
	if (izq < j)
		sort2(arr, izq ,j);
	if (i < der)
		sort2(arr, i ,der);
}
struct usuario leerarch(int pos){
	struct usuario a;
	char dat[200];
	ifstream p;
	p.open("usuarios.dat");
	p.seekg(pos,ios::cur);
	p.read(dat,200);
	p.close();
	string structura;
	string nomble;
	string nick;
	string email;
	string password;
	structura=dat;
	nomble=structura.substr(0,50);
	int cont=0;
	int i=49;
	while(nomble[i]==' '){
		cont++;
		i--;
	};
	nomble=nomble.substr(0,50-cont);
	a.nombre=nomble;
	nick=structura.substr(50,50);
	cont=0;
	i=49;
	while(nick[i]==' '){
		cont++;
		i--;
	};
	nick=nick.substr(0,50-cont);
	a.nickname=nick;
	email=structura.substr(100,50);
	cont=0;
	i=49;
	while(email[i]==' '&&i>=0){
		cont++;
		i--;
	};
	email=email.substr(0,50-cont);
	a.email=email;
	password=structura.substr(150,structura.length()-150);
	cont=0;
	i=password.length()-1;
	while(password[i]==' '){
		cont++;
		i--;
	};
	password=password.substr(0,50-cont);
	a.password=password;
	return a;

}
void creararchivodemenu(string dato,vector<struct menu>menu){
string nombre=dato+ ".dat";
ofstream archivo(nombre.c_str(),ios::out |ios::binary|ios::app);
int m=menu.size();
for(int i=0;i<m;i++){
	if(menu[i].confirmar==0){
		archivo.write(menu[i].plato.c_str(),50);
		archivo.write(menu[i].precio.c_str(),50);
		archivo.write(menu[i].stock.c_str(),50);
}
}

archivo.close();
}
void agregarplato(string dato){

	string nombre=dato+".dat";
	struct menu plato;
	ofstream archivo(nombre.c_str(),ios::out|ios::binary|ios::app);
 cout<<"Escriba el nombre del plato nuevo"<<endl;
 getline(cin,plato.plato);
 cout<<"Escriba precio del plato"<<endl;
 cin>>plato.precio;
 cout<<"Escriba numero de porciones disponibles"<<endl;
 cin>>plato.stock;
 archivo.write(plato.plato.c_str(),50);
 archivo.write(plato.precio.c_str(),50);
 archivo.write(plato.stock.c_str(),50);

}
void crearmenudesdecero(string dato){
	int n;
	struct menu menu;
	vector<struct menu>platos;
	cout<<"Ingrese nombre del plato"<<endl;
	cin.ignore();
	getline(cin,menu.plato);
	cout<<"Ingrese precio del plato"<<endl;
	cin>>menu.precio;
	cout<<"Ingrese cantidad en stock"<<endl;
	cin>>menu.stock;
	menu.confirmar=0;
	platos.push_back(menu);

		system("cls");
		cout<<"Su registro se ha logrado con exito.Gracias"<<endl;
		creararchivodemenu(dato,platos);
		int m;
		m=platos.size();

		for(int i=0;i<m;i++){
			platos[i].confirmar=1;
		}


		do{
			system ("cls");
			cout << "[1] Agregar otro plato" << endl;
			cout << "[2] Finalizar		   " << endl;
			cin >> n;
			switch(n){
			case 1: agregarplato(dato);
			break;
			case 2: system("pause");
			break;
			}
		}while(n<0 && n>3);
}
void ActualizaPlato(string dato, string plato, int n){
	string nombrearch = dato + ".dat";
	string nombre,precio,stock;
	fstream arch(nombrearch.c_str(), ios::out | ios::in |ios::binary);
	if (arch.is_open()){
             arch.seekp(n*150,ios::beg);
             cout<<"ingrese nuevo nombre de plato"<<endl;
             getline(cin,nombre);
             cout<<"Ingrese precio"<<endl;
             cin>>precio;
             cout<<"Ingrese cantidad"<<endl;
             cin>>stock;
             arch.write(nombre.c_str(),50);
             arch.write(precio.c_str(),50);
             arch.write(stock.c_str(),50);
	}
}
void MuestraPlato(string dato, vector <string> x){
	int g,m,p;
	string l;
	m = x.size();
	do{
		system("cls");
		for (int i=0; i<m; i++){
			cout << "[" << i+1 << "] " << x[i];
		}
		cout << "Seleccione el plato que desea cambiar";
		cin >> p;
		l = x[p-1];
	}while(p<0 && p>m);

	do{
		system("cls");
		cout << "[1]Actualizar Plato" << endl;
		//cout << "[2]Borrar Plato    " << endl;
		cin >> g;
		switch(g){
		case 1: system("pause");
				ActualizaPlato(dato,l, p-1);
		break;
		//case 2: BorraPlato(dato,l, p-1);
		//break;
		}
	}while(g<0 && g>2);


}

void listaplato(string dato){
	streampos inicio, fin;
	vector <string> x;
	string nombrearch = dato + ".dat";
	ifstream arch(nombrearch.c_str(), ios::in | ios::binary);
	if (arch.is_open()){
		inicio = arch.tellg();
		arch.seekg(0, ios::end);
		fin = arch.tellg();
		int i=0;
		char plato[50];
		arch.seekg(0, ios::beg);
		while (i<fin - inicio - 149){
			arch.seekg(i, ios::beg);
			arch.read(plato,50);
			x.push_back(plato);
			i = i + 150;
		}
	}
	else{
		cerr << "ERROR 404 NOT FOUND" << endl;
	}
		system("pause");
		MuestraPlato(dato,x);

}
void Comentarios(){
}
void Puntuacion(){
}
void Pantallaplato(string dato){

	int x;

	do{
		system("cls");
		cout << " [1]Agregar un nuevo plato 		" << endl;
		cout << " [2]Modificar plato existente	    " << endl;
		//cout << " [3]Atras							" << endl;
		cin >> x;
		switch (x){
			case 1: agregarplato(dato);
			break;
			case 2: listaplato(dato);
			break;
			//case 3: PantallaMenu();
		}
	}while(x<0 && x>3);
}

void PantallaMenu(string dato){

	int x;

	do{
		system("cls");
		cout << "*[1]Nuevo Menu		*" << endl;
		cout << "*[2]Agregar Plato  *" << endl;
		cout << "*[3]Atras			 " << endl;
		cin >> x;
		switch (x){
			case 1: crearmenudesdecero(dato);
			break;
			case 2: Pantallaplato(dato);
			break;
			//case 3: principalasociado();
			//break;
		}
	}while(x<0 && x>3);
}
void principalasociado(string dato){
	int n;
	do{
	system("cls");

	cout << "********** MENU PRINCIPAL **********" << endl;
	cout << "[1] Menu 		  **" << endl;
	//cout << "**[2] Reservas       **" << endl;
	cout << "[2] Comentarios  **" << endl;
	cout << "[3] Puntuacion	  **" << endl;
		cin >> n;
		switch(n){
			case 1: PantallaMenu(dato);
			break;
			//case 2: Reservas();
			//break;
			case 2: Comentarios();
			break;
			case 3: Puntuacion();
			break;
		}

	}
	while(n<0 && n>4);
}
bool comprobarusodeemail( string dato){
	bool usado;
		streampos inicio,fin;
	    ifstream archivo("asociados.dat",ios::in|ios::binary);
	   if(archivo.is_open()){
		inicio=archivo.tellg();
		archivo.seekg (0, ios::end);
		fin = archivo.tellg();

		int i;
		i=50;
		char email[50];

		archivo.seekg(0,ios::beg);

		do{
			archivo.seekg(i,ios::beg);
			archivo.read(email,50);



	        if(email==dato){
	        	usado=true;
	        	break;
	        	}
	        else{
	        	usado=false;
	        }
	        i=i+200;

		}while(i<fin-inicio-149);

	   }
	   else{
		   usado=false;
	   }
	   archivo.close();
		return usado;
}
bool comprobarusodeusername( string dato){
	bool usado;
		streampos inicio,fin;
	    ifstream archivo("asociados.dat",ios::in|ios::binary);
	   if(archivo.is_open()){
		inicio=archivo.tellg();
		archivo.seekg (0, ios::end);
		fin = archivo.tellg();

		int i;
		i=100;
		char username[50];

		archivo.seekg(0,ios::beg);

		do{
			archivo.seekg(i,ios::beg);
			archivo.read(username,50);



	        if(username==dato){
	        	usado=true;
	        	break;
	        	}
	        else{
	        	usado=false;
	        }
	        i=i+200;

		}while(i<fin-inicio-99);

	   }
	   else{
		   usado=false;
	   }
	   archivo.close();
		return usado;
}
bool comprobarusodeemail2( string dato){
	bool usado;
		streampos inicio,fin;
	    ifstream archivo("usuarios.dat",ios::in|ios::binary);
	   if(archivo.is_open()){
		inicio=archivo.tellg();
		archivo.seekg (0, ios::end);
		fin = archivo.tellg();

		int i;
		i=50;
		char email[50];

		archivo.seekg(0,ios::beg);

		do{
			archivo.seekg(i,ios::beg);
			archivo.read(email,50);



	        if(email==dato){
	        	usado=true;
	        	break;
	        	}
	        else{
	        	usado=false;
	        }
	        i=i+200;

		}while(i<fin-inicio-149);

	   }
	   else{
		   usado=false;
	   }
	   archivo.close();
		return usado;
}
bool comprobarusodeusername2( string dato){
	bool usado;
		streampos inicio,fin;
	    ifstream archivo("asociados.dat",ios::in|ios::binary);
	   if(archivo.is_open()){
		inicio=archivo.tellg();
		archivo.seekg (0, ios::end);
		fin = archivo.tellg();

		int i;
		i=100;
		char username[50];

		archivo.seekg(0,ios::beg);

		do{
			archivo.seekg(i,ios::beg);
			archivo.read(username,50);



	        if(username==dato){
	        	usado=true;
	        	break;
	        	}
	        else{
	        	usado=false;
	        }
	        i=i+200;

		}while(i<fin-inicio-99);

	   }
	   else{
		   usado=false;
	   }
	   archivo.close();
		return usado;
}
void muestralista(vector <string> x){ //Si hay error, priorizar esto xdxd
	int m,p;
	string l;
	m = x.size();
	do{
		for (int i=0; i<m; i++){
			cout << "[" << i+1 << "] " << x[i];
		}
		cout << "Seleccione el restaurant que desea cambiar";
		cin >> p;
		l = x[p-1];
		}while(p<0 && p>m);
		principalasociado(l);

}
void listarestaurant(string dato){ //Si hay error, priorizar esto xdxd x2.
	streampos inicio, fin;
	vector <string> x;

	ifstream arch("restaurantes.dat", ios::in | ios::binary);
	if (arch.is_open()){
		inicio = arch.tellg();
		arch.seekg(0, ios::end);
		fin = arch.tellg();
		int i=0;
		char username[50];
		char restaurant[50];
		arch.seekg(0, ios::beg);
		while (i<fin - inicio - 199){
			arch.seekg(i, ios::beg);
			arch.read(username,50);
				if (username == dato){
					arch.seekg(i+50,ios::beg);
					arch.read(restaurant,50);
					x.push_back(restaurant);
				}
			i = i + 200;
		}
	}
	else{
		cerr << "ERROR 404 NOT FOUND" << endl;
	}

}

void creararchivoderestaurant(vector<struct datosasociado>dato){
	ofstream asociados("restaurantes.dat",ios::out|ios::binary|ios::app);
	int s;
	s=dato.size();
	for(int i=0;i<s;i++){
		 int r=dato[i].restaurantes.size();
		 for(int j=0;j<r;j++){
			 if(dato[i].restaurantes[j].confirmar==0)
		    asociados.write(dato[i].username.c_str(),50);
            asociados.write(dato[i].restaurantes[j].nombre.c_str(),50);
			asociados.write(dato[i].restaurantes[j].ubicacion.c_str(),50);
			asociados.write(dato[i].restaurantes[j].telefono.c_str(),50);

		 }
	}
	asociados.close();
}
void creararchivodeasociado(vector<struct datosasociado>dato){
	ofstream asociados("asociados.dat",ios::out|ios::binary|ios::app);

	int s=dato.size();
	for( int i=0;i<s;i++){
		if(dato[i].confirmar==0){
	asociados.write(dato[i].nombre.c_str(),50);

	asociados.write(dato[i].correo.c_str(),50);
	asociados.write(dato[i].username.c_str(),50);
	asociados.write(dato[i].password.c_str(),50);
	}
	}
	asociados.close();
}
void asignar(struct usuario user ,string nom, string email, string nick, string pass){
	user.nombre=nom;
	user.email=email;
	user.nickname=nick;
	user.password=pass;
}
void registrousuario(){
	string nombre1;
	string email1;
	string nickname1;
	string password1;
	string password2;
	bool correcto=false;
	ofstream arch("usuarios.dat",ios::out|ios::app|ios::binary);
	bool f;
	int opcion,continuar;
	while(correcto==false){
		system("cls");
		cout<<"\t\t\tREGISTRO DE USUARIO"<<endl;
		cout<<"\t\t\t-------------------"<<endl;
		cout<<"Ingrese su nombre: ";
		cin.ignore();
		getline(cin,nombre1);
		cout<<"Ingrese su email: ";
		getline(cin,email1);
		do{
			if(comprobarusodeemail2(email1)){
				cout<<"Esa direccion de email ya esta siendo utilizado"<<endl;
				cout<<"Escriba otra direccion de email"<<endl;
				getline(cin,email1);
				f=false;
			}else{
				f=true;
			}
		} while(f==false);
		cout<<"----Presione 1 para continuar----"<<endl;
		cin>>continuar;
		system("cls");
		if(continuar==1){
			cout<<"Bienvenido "<<nombre1<<endl;
			cout<<"Ingrese un nombre de usuario: ";
			cin.ignore();
			getline(cin,nickname1);
			do{
				if(comprobarusodeusername2(nickname1)){
					cout<<"Ese nombre de usuario ya esta siendo utilizado"<<endl;
					cout<<"Escriba un otro nombre de usuario "<<endl;
					getline(cin,nickname1);
					f=false;
				}
				else{
					f=true;
				}
			} while(f==false);
			cout<<"Ingrese una contraseņa: ";
			char caracter;
			caracter=getch();
			password1="";
			while(caracter!=13){
				if(caracter!=8){
					password1+=caracter;
					cout<<"*";
				}else{
					if(password1.length()>0){
						cout<<"\b \b";
						password1=password1.substr(0,password1.length()-1);
					}
				}
				caracter=getch();
			}
			cout<<"\nConfirmar Password: ";
			caracter=getch();
			password2="";
			while(caracter!=13){
				if(caracter!=8){
					password2+=caracter;
					cout<<"*";
				}else{
					if(password2.length()>0){
						cout<<"\b \b";
						password2=password2.substr(0,password2.length()-1);
					}
				}
				caracter=getch();
			}
			if(password1==password2&&password1!=""){
				system("cls");
				cout<<nombre1<<endl;
				cout<<email1<<endl;
				cout<<nickname1<<endl;
				cout<<"\n[1]Confirmar datos"<<endl;
				cout<<"[2]Cambiar datos"<<endl;
				cout<<"\nEscoga una opcion: ";
				cin>>opcion;
				if(opcion==1){
					arch.write(nombre1.c_str(),50);
					arch.write(email1.c_str(),50);
					arch.write(nickname1.c_str(),50);
					arch.write(password1.c_str(),50);
					cout<<"\nRegistrado correctamente";
					correcto=true;
				}else {
					registrousuario();
				}
			}else{
				cout<<"\nLas contraseņas no coinciden"<<endl;
			}
		}
		cin.get();
	}
}
void registrodeasociado(){
	bool f;
	struct datosasociado asociado;
	vector<struct datosasociado>asociados;
	int siguiente;
	int continuar;
	string username2,correo2;
	cout<<"\t\t\tREGISTRO DE ASOCIADO"<<endl;
	cout<<"\t\t\t-------------------"<<endl;
	cout<<"Ingrese su Nombre: ";
	cin.ignore();
	getline(cin,asociado.nombre);
	cout<<"Ingrese su Correo: "<<endl;
	getline(cin,correo2);
	do{
				if(comprobarusodeemail(correo2)){
					cout<<"Esa direccion de email ya esta siendo utilizado"<<endl;
					cout<<"Escriba otra direccion de email"<<endl;
					getline(cin,correo2);
					f=false;
				}
				else{
					asociado.correo=correo2;
					f=true;
				}
			} while(f==false);
	cout<<"----Presione 1 para continuar----"<<endl;
	cin>>continuar;
	system("cls");
	string cadena=asociado.nombre;
	if(continuar==1){
		cout<<"Bienvenido "<<cadena<<endl;
		cout<<"Ingrese un nombre de usuario: "<<endl;
		cin>>username2;
		do{
			if(comprobarusodeusername(username2)){
				cout<<"Ese nomnbre de usuario ya esta siendo utilizado"<<endl;
				cout<<"Escriba un otro nombre de usuario "<<endl;
				cin>>username2;
				f=false;
			}
			else{
				asociado.username=username2;
				f=true;
			}
		} while(f==false);
		bool confirmar=false;
		do{
			cout<<"Password: ";
			char caracter;
			caracter=getch();
			string password1="";
			while(caracter!=13){
				if(caracter!=8){
					password1+=caracter;
					cout<<"*";
				}else{
					if(password1.length()>0){
						cout<<"\b \b";
						password1=password1.substr(0,password1.length()-1);
					}
				}
				caracter=getch();
			}
			cout<<"\nConfirmar Password: ";
			caracter=getch();
			asociado.password="";
			while(caracter!=13){
				if(caracter!=8){
					asociado.password+=caracter;
					cout<<"*";
				}else{
					if(asociado.password.length()>0){
						cout<<"\b \b";
						asociado.password=asociado.password.substr(0,asociado.password.length()-1);
					}
				}
				caracter=getch();
			}
			if(password1==asociado.password){
				confirmar=true;
			}else{
				cerr<<"\nLas contraseņas no coinciden"<<endl;
				confirmar=false;
			    system("cls");
			}
		}while(confirmar==false);
		cout<<" "<<endl;
		cout<<"----Presione 1 para continuar----"<<endl;
		cin>>continuar;
		system("cls");
		if(continuar==1){
			do{
				struct restaurant restaurant;
				cout<<"Ingrese nombre del restaurant:"<<endl;
				cin.ignore();
				getline(cin,restaurant.nombre);
				cout<<"Ingrese numero de telefono:"<<endl;
				getline(cin,restaurant.telefono);
				cout<<"Ingrese posicion del restaurant(segun mapa adjunto):"<<endl;
				getline(cin,restaurant.ubicacion);
				cout<<"Elija tipo de comida:"<<endl;
				cout<<"Criolla"<<endl;
				cout<<"Marina"<<endl;
				cout<<"China"<<endl;
				cout<<"Fastfood"<<endl;
				cout<<"Carnes"<<endl;
				cout<<"Vegetariana"<<endl;
				cin>>restaurant.tipodecomida;
				restaurant.confirmar=0;
				asociado.restaurantes.push_back(restaurant);
				cout<<"Presione 1 para registrar otro restaurant"<<endl;
				cout<<"Presione 2 para terminar el registro"<<endl;
				cin>>siguiente;
				system("cls");
			}while(siguiente==1);
		}
	}
	asociado.confirmar=0;
	int n=asociado.restaurantes.size();
		cout<<asociado.nombre<<endl;
		cout<<asociado.correo<<endl;
		cout<<asociado.username<<endl;
		cout<<asociado.password<<endl;
		for(int i=0;i<n;i++){
			cout<<asociado.restaurantes[i].nombre<<endl;
			cout<<asociado.restaurantes[i].telefono<<endl;
			cout<<asociado.restaurantes[i].ubicacion<<endl;
			cout<<asociado.restaurantes[i].tipodecomida<<endl;
		}
		int j;
	 cout<<"[1]Confirmar datos"<<endl;
	 cout<<"[2]Cambiar datos"<<endl;
	 cin>>j;
	 if(j==1){

		asociados.push_back(asociado);
		system("cls");

		cout<<"Su registro se ha logrado con exito.Gracias"<<endl;
		creararchivodeasociado(asociados);
		creararchivoderestaurant(asociados);
		int m;
		m=asociados.size();

		for(int i=0;i<m;i++){
			asociados[i].confirmar=1;
		}
		for(int i=0;i<n;i++){
			asociado.restaurantes[i].confirmar=1;
		}
	 }
	 else{
		 registrodeasociado();

	 }
		system("pause");
}
void comentar(struct restaurant rest){
	struct calificacion cal;
	struct comentarios com;
	system("cls");
	cout << "ingrese la puntuacion(0-5): " << endl;
	cal.usuario=userglb;
	cin >> cal.calificacion;
	system("cls");
	cout << "escriba su comentario: "<< endl;
	getline(cin,com.comentario);
	com.usuario=userglb;
	ofstream p1,p2;
	p1.open("comentarios.dat",ios::binary|ios::app);
	p1.write(rest.nombre.c_str(),50);
	p1.write(com.comentario.c_str(),100);
	p1.write(com.usuario.c_str(),50);
	p1.close();
	p2.open("calificacion.dat",ios::binary|ios::app);
	p2.write(rest.nombre.c_str(),50);
	p2.write(cal.calificacion.c_str(),1);
	p2.write(cal.usuario.c_str(),50);
	p2.close();
}

struct restaurant buscarest(string nombre){
	ifstream prest;
	int t,j=0;
	char a[150];
	struct restaurant aux;
	prest.open("restaurantes.dat",ios::binary);
	prest.seekg(0,ios::end);
	t=prest.tellg();
	prest.seekg(0,ios::beg);
	t=(t-prest.tellg())/150;
	for(int i=0;i<t;i++){
		j=0;
		prest.seekg(i*150,ios::beg);
		prest.read(a,150);
		while((j<50)&(a[j]!=' ')){
			aux.nombre[j]=a[j];
			j++;
		}
		if(aux.nombre ==nombre){
			j=50;
			while((j<100)&(a[j]!=' ')){
				aux.ubicacion=a[j];
				j++;
			}
			j=100;
			while((j<150)&(a[j]!=' ')){
				aux.telefono=a[j];
				j++;
			}
			aux.pos=i;
			break;
		}
		else{
			aux.nombre="-1";
		}
	}
	return aux;
}

void ranking(vector<string> filtros){
	ifstream pr,pr1;
	string aux;
	int t,j,r;
	int c[10];
	char a[150],b[101];
	pr.open("restaurant.dat",ios::binary);
	pr.seekg(0,ios::end);
	t=pr.tellg();
	pr.seekg(0,ios::beg);
	t=(t-pr.tellg())/150;
	struct restaurant vr[t];
	for(int i=0;i<t;i++){
		pr.read(a,150);
		j=50;
		while((j<100)&(a[j]!=' ')){
			vr[i].nombre[j]=a[j];
			j++;
		}
		j=100;
		while((j<150)&(a[j]!=' ')){
			vr[i].ubicacion[j]=a[j];
			j++;
		}
	}
	pr.close();
	//calcularcalifprom();
	sort1(vr,0,t);
	for(int i=0;i<10;i++){
		c[i]=calculoderutaminima(r,atoi(vr[i].ubicacion.c_str()));
	}
	for(int i=0;i<9;i++){
		if(c[i]>c[i+1]){
			t=i;
		}
	}
	system("cls");
	cout << "restaurant: " << vr[t].nombre << endl;
	system("pause");
}

void navegacionuser(void){
	system("cls");
	cout << "ingrese su ubicacion(mapa adjunto)" << endl;
	cin >> ub;
	system("cls");
	cout << "1: busqueda"<< endl;
	cout << "2: comentar"<< endl;
	int opc;
	cin >> opc;
	if(opc==1){
		vector<int> filtros;
		while(opc==1){
			system("cls");
			cout<<"SELECTOR DE FILTROS"<<endl;
			cout<<"1: PRECIO"<<endl;
			cout<<"2: CALIFICACION"<<endl;
			cin>>opc;
			filtros.push_back(opc);
			system("cls");
			cout << "ESCRIBA EL NUMERO DE OPCION"<< endl;
			cout << "1: escoger otro filtro" << endl;
			cout << "2: iniciar busqueda" << endl;
			cin >> opc;
		}
		//ranking(filtros);
	}
	else if(opc==2){
		string name;
		struct restaurant rest;
		system("cls");
		cout << "Ingrese el nombre: "<< endl;
		getline(cin,name);
		cin.ignore();
		rest=buscarest(name);
		if(rest.nombre=="-1"){
			system("cls");
			cout << "No se encontro restaurante"<< endl;
			cout << "1.volver a intentar        2.Finalizar"<< endl;
			cin>> rest.pos;
			if(rest.pos==1){
				navegacionuser();
			}
		}
		else{
			comentar(rest);
		}
	}
}
void loginasociado(){
	string user,password;
	int opcion;
	bool encontrado=false;
	while(!encontrado){
		system("cls");
		cout << "--------LOGIN DE USUARIO--------"<< endl;
		cout << "Usuario: ";
		getline(cin,user);
		cout << "Password: ";
		password="";
		char caracter;
		caracter=getch();
		while(caracter!=13){
			if(caracter!=8){
				password+=caracter;
				cout<<"*";
			}else{
				if(password.length()>0){
					cout<<"\b \b";
					password=password.substr(0,password.length()-1);
				}
			}
			caracter=getch();
		}
		streampos aux,tamanho;
	    ifstream puser("asociados.dat",ios::in|ios::binary);
		puser.seekg(0,ios::beg);
		aux=puser.tellg();
		puser.seekg(0,ios::end);
		tamanho=puser.tellg();
		int i=100;
		char auxiliar[50];
		char contra[50];
		bool usado;
		if(puser.is_open()){
			do{
				puser.seekg(i,ios::beg);
				puser.read(auxiliar,50);
		        if(user==auxiliar){
		        	i+=50;
		    		puser.seekg(i,ios::beg);
		    		puser.read(contra,50);
		    		i-=50;
		    		if(password==contra&&password!=""){
		    			listarestaurant(user);
		    			usado=true;
		    		}else {
		    			usado=false;
		    		}
		        } else{
		        	usado=false;
		        }
		        i=i+200;

			}while(i<tamanho-aux-149);
			puser.close();
			if(usado==false){
				cout<<"\nUsuario o Password incorrectos"<<endl;
			}
		}else {
			cout<<"No existen usuarios, por favor registrese"<<endl;
			cin.get();
		}
		if(usado==false){
			cout<<"[1]volver a intentar"<<endl;
			cout<<"[2]finalizar programa"<<endl;
			cout<<"Eliga una opcion: ";
			cin>>opcion;
			if(opcion==2){
				encontrado=true;
			}
		}
		cin.get();
	}
}
void loginusuario(){
	string user,password;
	int opcion;
	bool encontrado=false;
	while(!encontrado){
		system("cls");
		cout << "--------LOGIN DE USUARIO--------"<< endl;
		cout << "Usuario: ";
		getline(cin,user);
		cout << "Password: ";
		password="";
		char caracter;
		caracter=getch();
		while(caracter!=13){
			if(caracter!=8){
				password+=caracter;
				cout<<"*";
			}else{
				if(password.length()>0){
					cout<<"\b \b";
					password=password.substr(0,password.length()-1);
				}
			}
			caracter=getch();
		}
		streampos aux,tamanho;
	    ifstream puser("usuarios.dat",ios::in|ios::binary);
		puser.seekg(0,ios::beg);
		aux=puser.tellg();
		puser.seekg(0,ios::end);
		tamanho=puser.tellg();
		int i=100;
		char auxiliar[50];
		char contra[50];
		bool usado;
		if(puser.is_open()){
			do{
				puser.seekg(i,ios::beg);
				puser.read(auxiliar,50);
		        if(user==auxiliar){
		        	i+=50;
		    		puser.seekg(i,ios::beg);
		    		puser.read(contra,50);
		    		i-=50;
		    		if(password==contra&&password!=""){
		    			navegacionuser();
		    			usado=true;
		    		}else {
		    			usado=false;
		    		}
		        } else{
		        	usado=false;
		        }
		        i=i+200;

			}while(i<tamanho-aux-149);
			puser.close();
			if(usado==false){
				cout<<"\nUsuario o Password incorrectos"<<endl;
			}
		}else {
			cout<<"No existen usuarios, por favor registrese"<<endl;
			cin.get();
		}
		if(usado==false){
			cout<<"[1]volver a intentar"<<endl;
			cout<<"[2]finalizar programa"<<endl;
			cout<<"Eliga una opcion: ";
			cin>>opcion;
			if(opcion==2){
				encontrado=true;
			}
		}
		cin.get();
	}
}
void interfaz(){
	system("cls");
	int opcion,tipodeusuario;
	cout<<"----------RESTAURAPP----------"<<endl;
	cout <<"1) Iniciar sesion"<<endl;
	cout <<"2) Registrarse"<<endl;
	cout<<"----Elija una opcion----"<<endl;
	cin>>opcion;
	system("cls");
	if(opcion==2 ){
		cout<<"1)Usuario"<<endl;
		cout<<"2)Asociado"<<endl;
		cout<<"----Elija una opcion----"<<endl;
		cin>>tipodeusuario;
		system("cls");
		if(tipodeusuario==1){
			registrousuario();
			system("cls");
			interfaz();
		}
		else if(tipodeusuario==2){
			registrodeasociado();
			system("cls");
			interfaz();
		}

	}

	else if(opcion==1){
        cout<<"1)Usuario"<<endl;
		cout<<"2)Asociado"<<endl;
		cout<<"----Elija una opcion----"<<endl;
		cin>>tipodeusuario;
		system("cls");
		if (tipodeusuario==1){
		loginusuario();
	}
		else if(tipodeusuario==2){
			loginasociado();
		}
	}
}
int main() {
	system("color 1a");
	interfaz();
return 0;
}
